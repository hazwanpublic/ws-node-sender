/* eslint-disable linebreak-style */
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 3004;
const app = express();
var tempTime = false;

const qrcode = require('qrcode-terminal');
const repl = require('repl');

const { Client, LocalAuth, MessageMedia } = require('./index');

const client = new Client({
    // puppeteer: { headless: false }, 
    authStrategy: new LocalAuth()
});


app.use(cors());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));


console.log('Initializing...');

client.initialize();

client.on('qr', (qr) => {
    qrcode.generate(qr, {small: true});
    console.log('Please scan the QR code on the browser.');
});

client.on('authenticated', (session) => {
    console.log(JSON.stringify(session));
});

client.on('ready', () => {
    const shell = repl.start('wwebjs> ');
    shell.context.client = client;
    shell.on('exit', async () => {
        await client.destroy();
    });
});

client.on('message', async msg => {
    // console.log(msg);
    if (msg.body === '!asd') {
        // Send a new message as a reply to the current one
        msg.reply('pong');
    }else{
        // if(msg.body == '!group'){
        //     let chat = await msg.getChat();
        //     if (chat.isGroup) {
        //         msg.reply(`
        //             *Group Details*
        //             Name: ${chat.name}
        //             Description: ${chat.description}
        //             Created At: ${chat.createdAt.toString()}
        //             Created By: ${chat.owner.user}
        //             Participant count: ${chat.participants.length}
        //             Group id: ${chat.id._serialized}
        //         `);
        //     } else {
        //         msg.reply('This command can only be used in a group!');
        //     }
        // }
    }
});

client.on('message_create', async msg => {
    // Fired on all message creations, including your own
    if(msg.body == '!group'){
        let chat = await msg.getChat();
        if (chat.isGroup) {
            msg.reply(`
                *Group Details*
                Name: ${chat.name}
                Description: ${chat.description}
                Created At: ${chat.createdAt.toString()}
                Created By: ${chat.owner.user}
                Participant count: ${chat.participants.length}
                Group id: ${chat.id._serialized}
            `);
        } else {
            msg.reply('This command can only be used in a group!');
        }
    }
	
    // console.log(msg);
});

app.get('/', (req, res) => {
    res.send('Hello from server');
});

app.post('/wapi/broadcast', async(req, res, next) => {
    try {
        var raw = req.body;
        var data = JSON.parse(raw.data);

        let num = data[0].number+'@c.us';
        let msg = data[0].msg;

        console.log(data);
        let results = await client.sendMessage(num, msg); // open to sent actual message via whatsapp
        res.json(JSON.stringify(results)); // open to return value of sending actual message via whatsapp
        //res.json(JSON.stringify(data)) // open to return received data without actually sent message via whatsapp
        
    } catch (error) {
        next(error);
    }
});

app.post('/wapi/broadcastImg', async(req, res, next) => {
    try{
        var raw = req.body;
        
        console.log(raw);
        
        var data = JSON.parse(raw.data);
        var filename = data[0].filename;
        
        let num = data[0].number+'@c.us';
        num = num.replace(' ', '');
        num = num.replace('+', '');
        num = num.replace('-', '');
        let msg = data[0].msg;
        

        if (filename == ''){
            let results = await client.sendMessage(num, msg);
            res.json(JSON.stringify(results));
        }else {
            const media = MessageMedia.fromFilePath('./upload/'+filename);
            console.log(media);
            let results = await client.sendMessage(num, media, {caption: msg});
            res.json(JSON.stringify(results));
        }
        
        
    } catch (error) {
        if(!tempTime){
            tempTime = true;
            let d = new Date();
            let rtime = d.getTime();
            await client.sendMessage('60134340032@c.us', rtime + ' Error in console'); // alert dev when error occur
            setTimeout(() => {
                client.sendMessage('60194478597@c.us', rtime + ' Error in console'); // alert dev when error occur
            }, 10000);
        }else{
            setTimeout(() => { tempTime = false; }, 60000);
        }
        console.log('bimg', error);
        next(error);
    }
});

app.listen(PORT, () => {
    console.log(`Server running on localhost: ${PORT}`);
});